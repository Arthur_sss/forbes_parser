# Forbes parser

This repository contains script to parse data from forbes companies lists, enrich them with column contains company domain taken from Clearbit API.

### Tables

- `cloud100.csv` contains data from [Forbes Cloud 100 list](https://www.forbes.com/lists/cloud100/?sh=395086a27d9c)
- `global2000.csv` contains data from [Forbes Global 2000 list](https://www.forbes.com/lists/global2000/?sh=75f166fc5ac0)

### Usage

To use this script:

1. Insert the necessary link in the url string in `main.py`.
2. Rename the csv file in `main.py` as you want to (company list name preferrable).
3. Run `main.py`.

## License

This repository is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

