import requests
from bs4 import BeautifulSoup as bs
import csv

url = "https://www.forbes.com/lists/cloud100/?sh=395086a27d9c"
# url = "https://www.forbes.com/lists/global2000/?sh=75f166fc5ac0"


def get_company_domain(company_name):
    api = f"https://autocomplete.clearbit.com/v1/companies/suggest?query={company_name}"

    response = requests.get(api)
    if response.status_code == 200:
        try:
            return response.json()[0].get("domain")
        except:
            return "N/A"
    else:
        print(
            f"Failed to fetch data for {company_name}. Status code: {response.status_code}"
        )
        return None


response = requests.get(url)

if response.status_code == 200:
    soup = bs(response.text, "html.parser")
    table = soup.find_all("div", class_="table-row-group")
    table = bs(str(table), "html.parser")
    elements = table.find_all("a")

    csv_filename = "cloud100.csv"

    data = []

    for element in elements:
        text = element.get_text(" | ", strip=True)
        if len(text) > 1:
            row = text.split(" | ")
            data.append(row + [get_company_domain(row[1])])

    with open(csv_filename, "w") as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerows(data)
